# README

This is a CloudFormation stack that can be deployed to an AWS account. It creates, among other things, an SNS topic that will trigger an alarm when an event is published to it. It will send an SMS text message to a phone number, an email to multiple email addresses, and invoke a Lambda function that sets the value of an SSM parameter to `ALARM` and sends a Firebase notification that can be consumed by a mobile device (in this case, an Android app). The SSM parameter can be polled by other services to check the alarm state, and it will be switched back to `OK` automatically after roughly one minute by another Lambda function.

## Setup

1. Log in to an AWS account with Admin credentials.
2. Create a CloudFormation stack, using `cloudformation.yaml` as the stack template.

   - It will ask you for some parameters: provide a phone number (including country and area code), two email addresses that you control, and a Firebase key (Server key).
     - If you don't want to bother with any of these, delete all references to them in the template before deploying.
   - The email addresses will be sent a confirmation email, and the subscription must be confirmed before any more emails will be sent.
   - The texts and emails will be basic JSON events (SNS notification bodies), because I can't be bothered to do anything fancier.
   - Texts/emails will only be sent when it enters the `ALARM` state, phone notifications will happen when it switches back to `OK` as well.

3. Wait for the deployment to finish.
4. Navigate to the IAM console and generate access/secret key pairs for these users. Save these, as you'll need it for other things and won't be able to view it again (although you can always make a new one).

   - `WifeAlertButtonUser`
   - `WifeAlertPollingUser`

5. Wait for your wife to push the button.
